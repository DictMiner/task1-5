// used base structeres
#include <vector>
#include <list>

// input / output
#include <fstream>
#include <iostream>

// random & std::bind
#include <random>
#include <functional>

// for std::max, std::min of n values use:
#include <algorithm>
#include <initializer_list>

// read values from file
#include "reading.h" 


// Base edge of a graph structure (Road in our case)
struct Road {
        size_t from;     //from town
        size_t to;       //to town
        double width;     //road width
        const bool operator < (const Road& road) const
        {
            return width < road.width;
        }
        friend std::ifstream &operator >> (std::ifstream &in, Road &road) 
        { 
            in >> road.from >> road.to >> road.width; 
            return in; 
        } 
    };


// Base graph structure, store edges in list of paths from cities: paths_[i] - roads from i city.
class Roads
{
public:
    explicit Roads(const std::vector<Road> &roads);
    double FindMinWidthRoad();
    double FindMaxWidthWay(const size_t &start_city_idx, const size_t &end_city_idx);

private:
    size_t num_cities_;
    std::list< Road > *paths_;
};

/*  Generate base graph edges:
n_vertexes - number of vertexes in our graph
n_edges - number of expected edges

Generate unique pairs of <first_vertex, second_vertex> first_vertex < second_vertex with width in range(0, MAX_WIDTH).
Number of edges is n_edges or less if we got more than 5*n_edges tries. (Useful if n_edges ~ n_vertex*(n_vertex-1)/2 )
*/
std::vector<Road> GenerateValues(const size_t n_vertexes, const size_t n_edges)
{
    const double MAX_WIDTH = 100;
    std::vector<Road> values;
    values.reserve(n_edges);

    std::default_random_engine generator;
    std::uniform_int_distribution<int> vertex_distribution(0,n_vertexes-1);
    std::uniform_real_distribution<double> weight_distribution(0, MAX_WIDTH);

    auto vertex_dice = std::bind ( vertex_distribution, generator );
    auto weight_dice = std::bind ( weight_distribution, generator );
    std::vector<std::pair<size_t, size_t>> edges;
    edges.reserve(n_edges);
    size_t generated_edges = 0;
    size_t iterations = 0;
    while ((generated_edges < n_edges) && (iterations < 5*n_edges))
    {
        size_t first_vertex =  vertex_dice();
        size_t second_vertex = vertex_dice();
        double weight = weight_dice();

        if (first_vertex>second_vertex) {
            size_t temp_vertex = first_vertex;
            first_vertex = second_vertex;
            second_vertex = temp_vertex;
        }

        if (first_vertex != second_vertex) {
            bool new_edge = true;
            for (auto const &edge: edges) {
                if (edge == std::make_pair(first_vertex, second_vertex)) {
                    new_edge = false;
                    break;
                }
            }

            if (new_edge) {
                edges.push_back(std::make_pair(first_vertex, second_vertex));
                Road new_road = {first_vertex, second_vertex, weight};
                values.push_back(new_road);
                generated_edges += 1;
            }
        }
        iterations += 1;
        // limits number of tries to generate random edge
        if (iterations == 5*n_edges) {
            std::cout << "Genereted only " << values.size() << " edges";
        }
    }
    return values;
}


int main(int argc, char** argv) {

    std::string arg = argv[1];

    // generate test file, expected like -t test.txt args
    if (arg == "-t") {
        size_t start_city_idx, end_city_idx;
        std::vector<Road> roads_vec;
        std::string test_file;

        if (argc == 3) {
            test_file = argv[2];
        }

        if (argc == 5){
            test_file = argv[4];
        }

        if (!test_file.empty()) {
            std::ifstream iss(test_file);
            // read start city idx and end_city_idx from first line
            iss >> start_city_idx >> end_city_idx;

            // reread start city idx and end_city_idx from command line if provided
            if (argc == 5){
                start_city_idx = std::atoi(argv[2]);
                end_city_idx = std::atoi(argv[3]);
            }
            // reads roads information
            roads_vec = ReadValues<Road>(iss);
        }

        // loads Roads to graph class, find minimum roads width in graph and maximum roads width way from cities.
        Roads roads(roads_vec);
        double min_roads_width = roads.FindMinWidthRoad();
        double max_road_width = roads.FindMaxWidthWay(start_city_idx, end_city_idx);

        if ((max_road_width - min_roads_width) > 0) {
            std::cout << "Possible Armata's width: " << 0 << " - " << max_road_width - min_roads_width << "\n";
        }
        else {
            std::cout << "No possible Armata's width." << "\n";
        }
    }

    // generate test file, expected like -g 20 100 test.txt args
    if (arg == "-g"){
        size_t n_vertexes = std::atoi(argv[2]);
        size_t n_edges = std::atoi(argv[3]);
        std::string test_file = argv[4];

        std::vector<Road> roads_vec;
        if (!test_file.empty()) {
            std::ofstream iss(test_file);
            // started_city_idx end_city_idx in first row
            iss << 0 << ' ' << n_vertexes - 1 << "\n";
            roads_vec = GenerateValues(n_vertexes, n_edges);
            // number of edges in second row
            iss << roads_vec.size() << "\n";
            // fill next lines with edge information start_city end_city road_width
            for (auto const &road: roads_vec) {
                iss << road.from << ' ' << road.to << ' ' << road.width << "\n";
            }
        }
    }

    return 0;
}

// Initialize graph structure by vector of roads/ 
Roads::Roads(const std::vector<Road> &roads)
{
    num_cities_ = 0;
    for (auto const& road: roads) {
        num_cities_ = std::max({num_cities_, road.from, road.to});
    }
    num_cities_ += 1;

    paths_ = new std::list< Road >[num_cities_];

    for (auto const& road: roads)
    {
        paths_[road.from].push_back(road);
        Road road_reverse = {road.to, road.from, road.width};
        paths_[road.to].push_back(road_reverse);
    }
}

// Find minimum road width in our planet
double Roads::FindMinWidthRoad()
{
    double min_width = -1;
    for (size_t i=0; i<num_cities_; ++i)
    {
        for (auto const &path: paths_[i])
        {
            if (min_width == -1){
                min_width = path.width;
            }
            min_width = std::min(min_width, path.width);
        }
    }
    return min_width;
}

// Find maximum road width between cities
double Roads::FindMaxWidthWay(const size_t &start_city_idx, const size_t &end_city_idx)
{
    std::vector<double> widths(num_cities_, 0.0);
    std::vector<bool> is_city_visited(num_cities_, false);
    
    // initial width to start_city as maximum path from it, can be inf instead
    double max_width = 0;
    for (auto &path: paths_[start_city_idx])
    {
        max_width = std::max(max_width, path.width);
    }
    widths[start_city_idx] = max_width;

    for (size_t i=0; i<num_cities_; ++i)
    {
        // find not visited maximum width city
        size_t city_idx = num_cities_;
        double max_width = 0;
        for (size_t j=0; j<num_cities_; ++j)
        {
            if ((!is_city_visited[j]) && (widths[j]>max_width))
            {
                city_idx = j;
                max_width = widths[j];
            }
        }

        // break if reach end city or no more cities to visit 
        if ((city_idx == end_city_idx) || (max_width == 0))
        {
            break;
        }

        // move from max width road city
        for (auto &path: paths_[city_idx])
        {
            widths[path.to] = std::max(widths[path.to], std::min(max_width, path.width));
        }
        is_city_visited[city_idx] = true;
    }

    return widths[end_city_idx];
}

