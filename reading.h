#include <vector>


template <typename ValueType>
std::vector<ValueType> ReadValues(std::ifstream &input_stream = std::cin);


template <typename ValueType>
std::vector<ValueType> ReadValues(std::ifstream& input_stream)
{
    size_t size;
    input_stream >> size;
    ValueType value;
    std::vector<ValueType> values;
    values.reserve(size);

    for (size_t index = 0; index < size; ++index)
    {
        input_stream >> value;
        values.push_back(value);
    }
    return values;
}